$('.loading').hide();
$('.result').hide();
var parser = new UAParser();
var user_agent = parser.getResult();
var isMobile = false;
var cedula = {rut:'', nserie:'', nombres:'', apellidos:'', genero:'', nacionalidad: '', fnacimiento:'', fvencimiento:''};
var cedula_mrz = {rut:'', nserie:'', nombres:'', apellidos:'', genero:'', nacionalidad: '', fnacimiento:'', fvencimiento:''};
var documentData = {
    'national identification number':'',
    'document number':'',
    'name':'',
    'family name':'',
    'gender':'',
    'nationality': '',
    'date of birth':'',
    'expiration date':''
};
var cedula_mrz = {
    'national identification number':'',
    'document number':'',
    'name':'',
    'family name':'',
    'gender':'',
    'nationality': '',
    'date of birth':'',
    'expiration date':''
};
var direction = '';
var app_locale = '';
if (typeof user_agent.device.model != 'undefined' || user_agent.os.name == 'Android' || user_agent.os.name == 'iOS'
     || user_agent.os.name == 'Blackberry' || user_agent.os.name == 'Blackberry' || user_agent.os.name == 'Windows Phone'
     || user_agent.os.name == 'Windows Mobile') {
    console.log(user_agent);
    isMobile = true;
}

$(document).ready(function($) {
    app_locale = setLanguage();
    if (!isMobile) {
        var prefix;
        var version;
        if (window.mozRTCPeerConnection || navigator.mozGetUserMedia) {
            prefix = 'moz';
            version = parseInt(navigator.userAgent.match(/Firefox\/([0-9]+)\./)[1], 10);
        } else if (window.webkitRTCPeerConnection || navigator.webkitGetUserMedia) {
            prefix = 'webkit';
            version = navigator.userAgent.match(/Chrom(e|ium)/) && parseInt(navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)[2], 10);
        }
        if (prefix == 'moz' || prefix == 'webkit' && version > 41) {
            console.log("Browser Support WebRTC")
            navigator.mediaDevices.enumerateDevices().then(function (devices) {
                for(var i = 0; i < devices.length; i ++){
                    var device = devices[i];
                    if (device.kind === 'videoinput') {
                        var option = document.createElement('option');
                        option.value = device.deviceId;
                        option.text = device.label || 'camera ' + (i + 1);
                        document.querySelector('select#videoSource').appendChild(option);
                    }
                };
                setVideoStream($('#videoSource').val());
            });
            $('.card-body input[type="file"]').detach();
            $('label.images').detach();
        } else {
            console.log("This Browser Not Support WebRTC")
            bootbox.alert(lang[app_locale]['webrtc_fail']);
            $('.api').detach();
            $('.supported').show();
        }
    } else {
        $('.card.cam').children('.card-body').hide();
        $('.card-body button.btn.btn-dark').detach();
    }
});

$('body').on('click', '.navbar .lang button', function(event) {
    event.preventDefault();
    $(this).addClass('active');
    app_locale = $(this).attr('data-lang');
    setLocale($(this).attr('data-lang'));
});

$('body').on('click', '.take-id-front', function(event) {
    event.preventDefault();
    var photoData = takePhoto();
    $('img#id_front').attr('src', photoData);
    var base64data = $('img#id_front').attr('src').split(',');
    $('input[name="id_front"]').val(photoData);
});

$('body').on('click', '.take-id-back', function(event) {
    event.preventDefault();
    var photoData = takePhoto();
    $('img#id_back').attr('src', photoData);
    var base64data = photoData.split(',');
    $('input[name="id_back"]').val(photoData);
});

$('body').on('click', '.take-selfie', function(event) {
    event.preventDefault();
    var photoData = takePhoto();
    $('img#selfie').attr({ src : photoData, style: 'max-width: 319px;margin: 0 auto;'});
    var base64data = photoData.split(',');
    $('input[name="selfie"]').val(photoData);
});

$('body').on('change', 'input[type="file"]', function(event) {
    event.preventDefault();
    var img_id = $(this).parent().siblings('img').attr('id');
    var input_id = $(this).parent().siblings('input').attr('name');
    var canvas = document.getElementById('photo');
    var ctx = canvas.getContext('2d');
    // var cw = canvas.width;
    // var ch = canvas.height;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    // limit the image to 150x100 maximum size
    var maxW=1280;
    var maxH=720;
    var img = new Image;
    dataURL = null;
    img.onload = function() {
       var iw=img.width;
       var ih=img.height;
       var scale=Math.min((maxW/iw),(maxH/ih));
       var iwScaled=iw*scale;
       var ihScaled=ih*scale;
       canvas.width=iwScaled;
       canvas.height=ihScaled;
       ctx.drawImage(img,0,0,iwScaled,ihScaled);
       dataURL = canvas.toDataURL('image/jpeg');
       if (img_id == 'selfie') {
           $('img#selfie').attr({src: dataURL, style: 'max-width: 319px;margin: 0 auto;'});
       } else {
           $('img#' + img_id).attr('src', dataURL);
       }
       var base64data = dataURL.split(',');
       $('input[name="' + input_id + '"]').val(dataURL);
    }
    img.src = URL.createObjectURL(event.target.files[0]);
});

$('body').on('change', 'input[name="allCI"]', function(event) {
    event.preventDefault();
    locale = $('input[name="lang"]').val();
    $('.loading').show();
    if ($(this).is(":checked")) {
        setCountryOptions('', locale);
    } else {
        getGeoCountry(locale);
    }
    $('.loading').hide();
});

$('body').on('click', '.api-call', function(event) {
    event.preventDefault();
    resetData();
    cleanDataUI();
    $('.loading').show();
    $btn = $(this);
    $btn.prop('disabled', true);
    var data_valid = false;
    var documentType = $('select#documentType').val();
    if (documentType != 0) {
        var id_front = $('input[name="id_front"]').val();
        var id_back = $('input[name="id_back"]').val();
        var selfie = $('input[name="selfie"]').val();
        if (id_front.length > 0 && id_back.length > 0 && selfie.length > 0 && documentType != 'PASS') {
            data_valid = true;
        } else if (id_front.length > 0 && selfie.length > 0 && documentType == 'PASS') {
            data_valid = true;
        }
        if (data_valid) {
            var apiKey = '835efac757ed46c0937a30d94caaf2dd';
            var form_data = new FormData();
            form_data.append('id_front', dataURItoBlob(id_front));
            form_data.append('id_back', dataURItoBlob(id_back));
            form_data.append('selfie', dataURItoBlob(selfie));
            form_data.append('documentType', documentType);
            form_data.append('apiKey', apiKey);
            $.ajax({
                url: 'https://sandbox-api.7oc.cl/v2/face-and-document',
                async: true,
                crossDomain: true,
                processData: false,
                contentType: false,
                type: 'POST',
                data: form_data
            })
            .done(function(data) {
                // console.log(data);
                var status_class = '';
                var result_class = '';
                var result_text = '';
                var mrz = null;
                var code = null;
                var ocr = null;
                var type = null;
                var checksum_result = '';
                if (typeof(data["information from document"]) != 'undefined' && data["information from document"].length != 0) {
                    mrz = data["information from document"].mrz;
                    type = data["information from document"].type;
                    if (parseFloat(data["biometric result"]) > 1) {
                        result_class = 'text-success';
                        result_text = lang[app_locale]['positive'];
                    } else if (parseFloat(data["biometric result"]) == 1) {
                        result_class = 'text-warning';
                        result_text = lang[app_locale]['positive'];
                    } else if (parseFloat(data["biometric result"]) < 0) {
                        result_class = 'text-warning';
                        result_text = lang[app_locale]['positive'];
                    } else if (parseFloat(data["biometric result"]) == 0) {
                        result_class = 'text-danger';
                        result_text = lang[app_locale]['negative'];
                    }
                    if (data.status == '200' || data.status == '201' || data.status == '202') {
                        status_class = 'badge-success';
                    } else {
                        status_class = 'badge-danger';
                    }
                    $('.card.match h1.confidence').text(result_text).addClass(result_class);
                    $('.card.match h6 span.status').text(statusCode[app_locale][data.status]).addClass(status_class);
                    $('.card.match p.token span.badge-light').text(data.toc_token);
                    if (mrz != null && typeof(mrz) != 'undefined' && typeof(mrz.checksum) != 'undefined') {
                        if (mrz.checksum == '1' && JSON.stringify(mrz.data) != 'undefined') {
                            checksum_result = lang[app_locale]['checksum_positive'] + '_success';
                        } else {
                            checksum_result = lang[app_locale]['checksum_negative'] + '_warning';
                            if (JSON.stringify(mrz) != 'undefined' && typeof(mrz.raw) != 'undefined') {
                                processRawMRZ(mrz.raw);
                                $('.card.info-doc h6.mrz-raw').show();
                            }
                        }
                    } else {
                        checksum_result = lang[app_locale]['checksum_negative'] + '_warning';
                    }
                    checksum_result = checksum_result.split('_');
                    $('.card.info-doc p.mrz-check span.badge').text(checksum_result[0]).addClass('badge-' + checksum_result[1]);
                    // check old CI
                    if (typeof(data["information from document"].code) != 'undefined' && data["information from document"].code.length != 0) {
                        code = data["information from document"].code;
                        if (typeof(code["document status"]) != 'undefined' && code["document status"].length != 0) {
                            $('a.link-rc').attr({href: code["document status"], target: '_blank'}).show();
                        }
                        if (documentType == 'DCAN') {
                            $('.card.match .card-body p.mrz-check').hide();
                            $('.card.match .card-body ul.list-group-flush.mrz-data').hide();
                            $.each(code.data, function(index, el) {
                                var data = '<li class="list-group-item">' + index + ': <strong>' + el + '</strong></li>';
                                $('.card.match .card-body ul.list-group-flush.data').append(data);
                            });
                            $('.card.match .card-body ul.list-group-flush.data').show();
                        } else if (documentType == 'GBR1') {
                            $('.card.match .card-body p.mrz-check').hide();
                            $('.card.match .card-body ul.list-group-flush.mrz-data').hide();
                            $.each(ocr.data, function(index, el) {
                                var data = '<li class="list-group-item">' + index + ': <strong>' + el + '</strong></li>';
                                $('.card.match .card-body ul.list-group-flush.data').append(data);
                            });
                            $('.card.match .card-body ul.list-group-flush.data').show();
                        } else {
                            if (mrz != null && typeof(mrz.data) != 'undefined') {
                                setDocumentData(code, mrz.data);
                            } else {
                                setDocumentData(code, cedula_mrz);
                            }
                        }
                    } else {
                        if (mrz != null && JSON.stringify(mrz.data) != 'undefined') {
                            setDocumentData('', mrz.data);
                        } else {
                            setDocumentData('', cedula_mrz);
                        }
                    }

                    if (documentData["national identification number"] == ''
                        || documentData["national identification number"] == 'undefined'
                        || documentData["national identification number"] == undefined) {
                        $('li.list-group-item.rut strong').text(documentData["document number"]);
                    } else {
                        $('li.list-group-item.rut strong').text(documentData["national identification number"]);
                        $('li.list-group-item.n-serie strong').text(documentData["document number"]);
                    }

                    $('li.list-group-item.names strong').text(documentData.name);
                    $('li.list-group-item.lastnames strong').text(documentData["family name"]);
                    $('li.list-group-item.gender strong').text(documentData.gender);
                    $('li.list-group-item.country strong').text(documentData.nationality);
                    $('li.list-group-item.date-birth strong').text(moment(documentData["date of birth"], "YY/MM/DD").format("DD/MM/YYYY"));
                    $('li.list-group-item.date-expire strong').text(moment(documentData["expiration date"], "YY/MM/DD").format("DD/MM/YYYY"));
                    $btn.prop('disabled', false);
                    $('.loading').hide();
                    $('.result').show();
                } else {
                    $btn.prop('disabled', false);
                    $('.loading').hide();
                    bootbox.alert(lang[app_locale]['images_error']);
                }
            })
            .fail(function(data) {
                console.log(data);
                console.log("error");
                bootbox.alert(lang[app_locale]['api_error']);
                $btn.prop('disabled', false);
                $('.loading').hide();
            });
        } else {
            $btn.prop('disabled', false);
            $('.loading').hide();
            bootbox.alert(lang[app_locale]['images_error']);
        }
    } else {
        $btn.prop('disabled', false);
        $('.loading').hide();
        bootbox.alert(lang[app_locale]['document_empty']);
    }
});

function processRawMRZ(mrzRaw) {
    var decode_mrz = mrzRaw.split('\\n');
    if (decode_mrz[0].substr(0, 5) == 'INCHL') { // new CI
        cedula_mrz.nserie = decode_mrz[0].substr(5, 9);
        cedula_mrz.fnacimiento = decode_mrz[1].substr(0, 6);
        cedula_mrz.genero = decode_mrz[1].substr(7, 1);
        cedula_mrz.fvencimiento = decode_mrz[1].substr(8, 6);
        cedula_mrz.nacionalidad = decode_mrz[1].substr(15, 3);
        cedula_mrz.rut = decode_mrz[1].split(/</)[0].substr(18, decode_mrz[1].split(/</)[0].length) + decode_mrz[1].split(/</)[1];
        cedula_mrz.apellidos = decode_mrz[2].split(/<</)[0].replace(/</g, ' ');
        cedula_mrz.nombres = decode_mrz[2].split(/<</)[1].replace(/</g, ' ');
    } else if (decode_mrz[0].substr(0, 5) == 'IDCHL') { // old CI
        cedula_mrz.rut = decode_mrz[0].substr(5, 9);
        cedula_mrz.fnacimiento = decode_mrz[1].substr(0, 6);
        cedula_mrz.genero = decode_mrz[1].substr(7, 1);
        cedula_mrz.fvencimiento = decode_mrz[1].substr(8, 6);
        cedula_mrz.nacionalidad = decode_mrz[1].substr(15, 3);
        cedula_mrz.nserie = decode_mrz[1].split(/</)[0].substr(18);
        cedula_mrz.apellidos = decode_mrz[2].split(/<</)[0].replace(/</g, ' ');
        cedula_mrz.nombres = decode_mrz[2].split(/<</)[1].replace(/</g, ' ');
    }
    // UI element MRZ RAW in 3 lines
    $('.card.info-doc h6.mrz-raw span.one').text(decode_mrz[0]);
    $('.card.info-doc h6.mrz-raw span.two').text(decode_mrz[1]);
    $('.card.info-doc h6.mrz-raw span.three').text(decode_mrz[2]);
}

function setDocumentData(code, mrz) {
  try{
			console.log('code.data '+JSON.stringify(code.data));
					console.log('mrz '+JSON.stringify(mrz));
    if (code != '') {
      documentData = JSON.parse(JSON.stringify(code.data));
      documentData = JSON.parse(JSON.stringify(mrz));
      documentData["national identification number"] = code.data["national identification number"];
      documentData["document number"] = code.data["document number"];
      /*
        $.each(code.data, function(index, el) {
            documentData[index] = el;
        });
        $.each(documentData, function(key, value) {
            if (value == '') {
                documentData[key] = mrz[key];
            }
        });
      */
    } else {
      documentData = JSON.parse(JSON.stringify(mrz));
      /*
        $.each(mrz, function(index, el) {
            documentData[index] = el;
        });
      */
    }
  }
  catch(e) {
    console.log('ERRORprocessRawMRZ: '+e)
  }
  return documentData;
}

function resetData() {
    documentData = {
        'national identification number':'',
        'document number':'',
        'name':'',
        'family name':'',
        'gender':'',
        'nationality': '',
        'date of birth':'',
        'expiration date':''
    };
    cedula_mrz = {
        'national identification number':'',
        'document number':'',
        'name':'',
        'family name':'',
        'gender':'',
        'nationality': '',
        'date of birth':'',
        'expiration date':''
    };
}

function cleanDataUI() {
    // reset driving License
    $('.card.info-doc p.mrz-check').show();
    $('.card.info-doc ul.list-group-flush.mrz-data').show();
    $('.card.info-doc ul.list-group-flush.data').hide();
    // clean
    if ($('.card.match h1.confidence').hasClass('text-success')) {
        $('.card.match h1.confidence').removeClass('text-success');
    } else {
        $('.card.match h1.confidence').removeClass('text-danger');
    }
    $('.card.match h1.confidence').text('');
    if ($('.card.match h6 span.status').hasClass('badge-success')) {
        $('.card.match h6 span.status').removeClass('badge-success');
    } else {
        $('.card.match h6 span.status').removeClass('badge-danger');
    }
    $('.card.match h6 span.status').text('');
    $('.card.match p.token span.badge-light').text('');
    $('.card.info-doc p.mrz-check span.badge').text('');
    if ($('.card.info-doc p.mrz-check span.badge').hasClass('badge-success')) {
        $('.card.info-doc p.mrz-check span.badge').removeClass('badge-success');
    } else {
        $('.card.info-doc p.mrz-check span.badge').removeClass('badge-warning');
    }
    $('.card.info-doc h6.mrz-raw span.one').text('');
    $('.card.info-doc h6.mrz-raw span.two').text('');
    $('.card.info-doc h6.mrz-raw span.three').text('');
    $('.card.info-doc h6.mrz-raw').hide();
    $('.card.info-doc a.link-rc').attr('href', '#').hide();
    $('li.list-group-item.rut').show();
    $('li.list-group-item.rut strong').text('');
    $('li.list-group-item.n-serie strong').text('');
    $('li.list-group-item.names strong').text('');
    $('li.list-group-item.lastnames strong').text('');
    $('li.list-group-item.gender strong').text('');
    $('li.list-group-item.country strong').text('');
    $('li.list-group-item.date-birth strong').text('');
    $('li.list-group-item.date-expire strong').text('');
}

$('body').on('change', 'select#videoSource', function(event) {
    event.preventDefault();
    deviceId = $(this).val();
    setVideoStream(deviceId);
});

$('body').on('click', '.rotate', function(event) {
    event.preventDefault();
    rotatedBase64 = '';
    direction = $(this).attr('data-dir');
    input_name = $(this).parent().siblings('input[type="text"]').attr('name');
    img_id = $(this).parent().siblings('img.card-img-top').attr('id');
    imageSrc = $(this).parent().siblings('img.card-img-top').attr('src');
    if (imageSrc.length > 0) {
        $('input[name="' + input_name + '"]').val('');
        rotate64(imageSrc).then(function(rotated) {
            if (img_id == 'selfie') {
                $('img#selfie').attr({src: rotated, style: 'max-width: 319px;margin: 0 auto;'});
            } else {
                $('img#' + img_id).attr('src', rotated);
            }
            // var base64data = rotated.split(',');
            $('input[name="' + input_name + '"]').val(rotated);
    	}).catch(function(err) {
    		console.error(err);
    	});
    }
});

var rotate64 = function(base64data, degrees, enableURI) {
	return new Promise(function(resolve, reject) {
		//assume 90 degrees if not provided
        var degrees = 90;
        if (direction == 'left') {
            degrees = -90;
        }
		var canvas = document.createElement('canvas');
		canvas.setAttribute('id', 'hidden-canvas');
		canvas.style.display = 'none';
		document.body.appendChild(canvas);
        var ctx = canvas.getContext('2d');
        var image = new Image();
        //assume png if not provided
        image.src = base64data;
        image.onload = function() {
            var w = image.width;
            var h = image.height;
            var rads = degrees * Math.PI/180;
            var c = Math.cos(rads);
            var s = Math.sin(rads);
            if (s < 0) { s = -s; }
            if (c < 0) { c = -c; }
            //use translated width and height for new canvas
            canvas.width = h * s + w * c;
            canvas.height = h * c + w * s;
            //draw the rect in the center of the newly sized canvas
            ctx.translate(canvas.width/2, canvas.height/2);
            ctx.rotate(degrees * Math.PI / 180);
            ctx.drawImage(image, -image.width/2, -image.height/2);
            //assume plain base64 if not provided
            resolve(canvas.toDataURL('image/jpeg'));
            document.body.removeChild(canvas);
        };
        image.onerror = function() {
            reject('Unable to rotate data\n' + image.src);
        };
    });
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = decodeURIComponent(dataURI.split(',')[1]);
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type:'image/jpeg'});
}

function setVideoStream(deviceId) {
    // console.log(deviceId);
    var constraints = { audio: false, video: { width: 1280, height: 720, deviceId: deviceId} };
    navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream) {
        var video = document.querySelector('video');
        var preview = $('video#preview');
        video.srcObject = mediaStream;
        video.onloadedmetadata = function(e) {
            video.play();
        };
    }).catch(function(err) {
        bootbox.alert(lang[app_locale]['camera_error']);
        console.log(err.name + ": " + err.message);
    }); // always check for errors at the end.
}

function setCountryOptions(country, locale) {
    $('#documentType').find('option').remove();
    var ci_codes = '';
    if (locale === 'es') {
        ci_codes = ci_codes_es;
    } else if (locale === 'en') {
        ci_codes = ci_codes_en;
    }
    if (typeof country != 'undefined' && country != '' && country != null && country.length > 0) {
        $('label[for="documentType"]').text(lang[app_locale]['document_type'] + ' ' + country + ' : ');
        // set especific options
        $.each(ci_codes, function(index, element) {
            if (element.hasOwnProperty(country)) {
                $.each(element[country], function(index, item) {
                    $('#documentType').append($('<option>', {
                        value: item.value,
                        text : item.name
                    }));
                });
            }
        });
    } else {
        // set all options
        $('label[for="documentType"]').text(lang[app_locale]['document_type'] + ' : ');
        $('#documentType').append($('<option>', {
            value: 0,
            text : lang[app_locale]['select_document']
        }));
        $.each(ci_codes, function(index, element) {
            for (var key in element) {
                // console.log(key);
                $.each(element[key], function(index, item) {
                    if (item.value != 0) {
                        $('#documentType').append($('<option>', {
                            value: item.value,
                            text : item.name + ' ' + key
                        }));
                    }
                });
            }
        });
    }
}

function setTemplateCountry(country) {
    var base_image = '';
    base_image = (window.location.href.indexOf('cmr') !== -1) ? '../img/' : 'img/';
    switch (country) {
        case 'Chile':
            $('img#id_front').attr({src: base_image + 'ci_front.png', style: 'max-width: 320px;margin: 0 auto;'});
            $('img#id_back').attr({src: base_image + 'ci_back.png', style: 'max-width: 320px;margin: 0 auto;'});
            $('.card.id-front .card-header h4').text(lang[app_locale]['cl_front']);
            $('.card.id-back .card-header h4').text(lang[app_locale]['cl_back']);
            break;
        case 'Peru':
            $('img#id_front').attr({src: base_image + 'dni_front_peru.png', style: 'max-width: 325px;margin: 0 auto;'});
            $('img#id_back').attr({src: base_image + 'dni_back_peru.png', style: 'max-width: 325px;margin: 0 auto;'});
            $('.card.id-front .card-header h4').text(lang[app_locale]['per_front']);
            $('.card.id-back .card-header h4').text(lang[app_locale]['per_back']);
            break;
        case 'Colombia':
            $('img#id_front').attr({src: base_image + 'ci_front_col.png', style: 'max-width: 322px;margin: 0 auto;'});
            $('img#id_back').attr({src: base_image + 'ci_back_col.png', style: 'max-width: 322px;margin: 0 auto;'});
            $('.card.id-front .card-header h4').text(lang[app_locale]['col_front']);
            $('.card.id-back .card-header h4').text(lang[app_locale]['col_back']);
            break;
        default:
            $('img#id_front').attr({src: base_image + 'id-card.jpg', style: 'max-width: 206px;margin: 0 auto;'});
            $('img#id_back').attr({src: base_image + 'id_card_back.svg', style: 'max-width: 206px;margin: 0 auto;'});
            $('.card.id-front .card-header h4').text(lang[app_locale]['all_front']);
            $('.card.id-back .card-header h4').text(lang[app_locale]['all_back']);
    }
}

function takePhoto() {
    var video = document.getElementById('video');
    var canvas = document.getElementById('photo');
    canvas.getContext('2d').drawImage(video, 0, 0, 1280, 720, 0, 0, 1280, 720);
    return canvas.toDataURL('image/jpeg');
}

function getGeoCountry(locale) {
    var country = '';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng='
                + position.coords.latitude + ','
                + position.coords.longitude)
            .done(function(data, textStatus, jqXHR) {
                if ( textStatus === 'success' ) {
                    if (typeof data.results[data.results.length -1].formatted_address != 'undefined') {
                        country = data.results[data.results.length -1].formatted_address;
                    }
                    setCountryOptions(country, locale);
                }
            });
        });
    } else {
        setCountryOptions(country, locale);
    }
}

function setLanguage() {
    var language = navigator.language.split('-');
    setLocale(language[0]);
    return language[0];
}

function setLocale(locale) {
    $('input[name="lang"]').val(locale);
    $('.checkbox .col-form-label span').text(' ' + lang[locale]['all_documents']);
    var path_prefix = '../locale/';
    if (window.location.href.indexOf('consorcio') >= 0) {
        path_prefix = '../../locale/';
    }
    var opts = { language: locale, pathPrefix: path_prefix, skipLanguage: 'es-CL' };
    $('[data-localize]').localize('app', opts);
    getGeoCountry(locale);
}
