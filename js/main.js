$('.loading').hide();
var interval = null;
var nistIndex = "";
var resultTransaction = 0;
var rutTransaction = "";
var documentType = "";

$(document).ready(function () {
    //restartApp();
	cleanTxtIE();
    connect(0);
    blinkText($(".feedback"));
    $("#success").hide();
    $("#info").hide();
    $("#error").hide();
    if (consulta_estado_ci) {
        $("#div-consultar-estado-ci").show();
    }
    if (consulta_estado_dispositivos) {
        $("#div-obtener-estados").show();
    }
});

function iniciaServicio(){
    $.ajax({
        url: 'https://localhost:41370/init',
        type: 'POST'
    })
    .done(function(data) {
        console.log("servicio iniciado");
        console.log( data);
    })
    .fail(function() {
        console.log("error al iniciar servicio");
    })
    .always(function() {
        console.log("complete");
    });
}

function detieneServicio(){
    console.log("detieneServicio")
    $.ajax({
        url: 'https://localhost:41370/stop',
        type: 'POST'
    })
    .done(function(data) {
        console.log("servicio detenido");
        console.log(data)
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

$(document).on("click", '#download-image2', function (e) {
    e.preventDefault();
    var images = $(".image");

    var link = document.createElement('a');
    link.download = "imagen2.png";
    link.href = images[1].src;
    link.click();
});


$("#consultar-estados").click(function (event) {
    event.preventDefault();
    actualizarEstadoDispositivos();
});


$('#read-bip').click(function (e) {
    modo_foto = 'default';
    e.preventDefault();
    TOC.emit('iniciarLecturaNFC');
});

$("#obtener-estados").click(function (e) {
    console.log('> obtener estados');
    interval = window.setInterval(function () {
        TOC.invoke('obtenerEstadoDeDispositivos', "3;1;2");
    }, 3000);

});

$('#captura-foto').click(function (e) {
    console.log("capturarFotografia");
    e.preventDefault();
    TOC.emit('iniciarCapturaFotografia');

    $('#captura-foto').toggleClass('disabled');
    $('#capturando-foto').toggleClass('disabled');
});



$('#how').click(function (e) {
    $('.carousel').carousel(0);
});

$('#stream-start').click(function (e) {
    e.preventDefault();
    TOC.emit('iniciarStream');
});

$('#stream-stop').click(function (e) {
    e.preventDefault();
    TOC.emit('detenerStream');
    $('#stream-container').html('');
});

$('#connect').click(function (e) {
    e.preventDefault();
    connect(0);
});

$('#disconnect').click(function (e) {
    e.preventDefault();
    disconnect();
});

$(window).on("unload", function(e) {
    detieneServicio();
});

$('#new-verification').click(function (e) {
    e.preventDefault();
    restartApp();
});

$('#call-previred').click(function (e) {
    console.log("> call-previred");
    e.preventDefault();
    TOC.invoke('iniciarLlamadaPrevired', [user_previred, pass_previred, run, id_trx]);
});

$('#consultar-ci').click(function (e) {
    console.log("> consultar CI");
    e.preventDefault();
    $('#estadoCIMensaje').html("Obteniendo el estado de su cédula. Espere unos momentos... <img src='img/spinner.gif' style='width: 20px;' alt='Cargando...'>");

    var rut = $('#scanned_rut').val();
    var serie = $('#scanned_serie').val();
    var transaccion = id_trx;
    TOC.invoke('consultarEstadoCI', rut + ";" +serie + ";"+ transaccion);
});


$('#get-data').click(function (e) {
    console.log("getDatosEquipo");
    e.preventDefault();
    $('#pcInfoMessage').html("Obteniendo los datos del equipo. Espere unos momentos... <img src='img/spinner.gif' style='width: 20px;' alt='Cargando...'>");
    TOC.emit('obtenerDatosEquipo');
});


$(document).on("click", '#download-image1', function (e) {
    e.preventDefault();
    var images = $(".image");

    var link = document.createElement('a');
    link.download = "imagen1.png";
    link.href = images[0].src;
    link.click();
});


function connect(_acto) {
    console.log("connect executed with act " + _acto);
    iniciaServicio();

    TOC.exec(_acto, {
        "notificacionCodigoLeido": "funcionCodigoLeido",
        "notificacionHuellaLeida": "funcionHuellaLeida",
        "notificacionVerificado": "funcionVerificado",
        "notificacionError": "funcionError",
        "notificacionFotografiaObtenida": "funcionFotografiaObtenida",
        "notificacionInformativo": "funcionInformativo",
        "notificacionStreamObtenido": "funcionStreamObtenido",
        "notificacionEstadoDeDispositivos": "funcionEstadoDispositivos",
        "notificacionPreviredObtenido": "funcionPreviredObtenido",
        "notificacionEstadoCIObtenido": "funcionEstadoCIObtenido",
        "notificacionDatosEquipo": "funcionDatosEquipo"
    });
}

function disconnect() {
    console.log("disconnect executed");
    TOC.disconnect();
}

$("#confirm-facial-goes").click(function(){
    // $("#id_back").attr("src", "data:image/jpeg;base64," + foto_cedula)
    $(".feedback #msj").html('Tome las tres fotografías indicadas y presione "Verificar" para comenzar la verificación.');
    $(".feedback .header").html('VERIFICACIÓN FACIAL');
    $("#error").hide();
    $("#info").hide();
    $("#success").hide();
    $("#verification-feedback").html("");
    $("#new-verification").hide();
    $("#call-previred").hide();
    $('#dactilar').hide();
    $('#facial').show();
    $('#facial-panel').show();
    $("#modal-confirm-facial").modal("hide");
    setTimeout(startVideoStream(), 3000);
});

$("#discard-facial").click(function(){
    connect(0);
});

function funcionCodigoLeido(message) {
    restartAppNoForm();
    tipo_carnet = '';

    $(".feedback .header").html("PASO 2");
    $(".feedback #msj").html("Coloque el dedo indicado en el lector de huella");

    var objectNotificacionCodigoLeido = $.parseJSON(message);

    console.log("funcionCodigoLeido");
    console.log(objectNotificacionCodigoLeido);

    if (objectNotificacionCodigoLeido.extra.serie.indexOf("A") == -1) {
        documentType = "CHL2";
    } else{
        documentType ="CHL1";
    }
    console.log(documentType);

    if (!objectNotificacionCodigoLeido.estado && objectNotificacionCodigoLeido.extra.nombres != "CHIP NO LEÍDO") {
        notificar('error', objectNotificacionCodigoLeido.mensaje.mensaje_usuario);
    }

    $.each(objectNotificacionCodigoLeido.extra, function (key, val) {

        if (key === "vencida") {
            // False => No se encuentra vencida
            $('#scanned_vencimiento').addClass((val === "False" ? "" : "cedulaVencida"));
        }
        else if (key === "dedo") {
            $('#row-finger').fadeIn(500);

            $('#fingersToScan').css("visibility", "visible");
            $('#fingersToScan').empty().html(val);

            repetirDedo(val);
            $('#fingersToScan').show();
            $('#row-finger').fadeIn(500);
        }
        else {
            $('#scanned_' + key).val(val);
        }
    });
    rutTransaction = objectNotificacionCodigoLeido.extra.rut;
    var txt_val = new Array();
    var nombre_archivo = $("#scanned_rut").val() + ".txt";
    var cont = 0;
    $("form#info-persona .form-group").each(function () {
        var label = $(this).find("label").html();
        var input = $(this).find("input").val();
        txt_val[cont] = label + " " + input;
        cont++;
    });

    var serie = objectNotificacionCodigoLeido.extra.serie;
    if (serie.charAt(0) == 'A') {
        tipo_carnet = 'antiguo';
        dedo = objectNotificacionCodigoLeido.extra.dedo;
    } else {
        dedo = objectNotificacionCodigoLeido.extra.dedo;
    }

    $('#fingerContainer').css('border', '15px solid #008cba');
    $("#success").fadeOut(500);
    $("#error").fadeOut(500);
    $("#info").fadeOut(500);
    $('#dedoUsado').fadeOut(500);
    $("#verification-feedback").fadeOut(500);
    $("#new-verification").hide();
    $('#fingersToScanImage').attr("src", "img/fingerprint.png");
    $('#fingersToScanImage').attr("style", "class='imagen'");
};

function funcionHuellaLeida(message) {

    $('#verificationIcon').show();
    $('#verificationIcon').attr('src', 'img/Cube2.gif');
    $("#verification-feedback").hide();

    $("#info").hide();

    var objectNotificacionHuellaLeida = $.parseJSON(message);
    console.log("funcionHuellaLeida");
    console.log(objectNotificacionHuellaLeida);

    $('#fingerContainer').css('border', '15px solid #a5dc85');

    if (!objectNotificacionHuellaLeida.estado) {
        notificar('error', objectNotificacionHuellaLeida.mensaje.mensaje_usuario);
    }

    $('#fingersToScanImage').attr('src', 'data:image/png;base64,' + objectNotificacionHuellaLeida.extra.imagen);

    $('#fingersToScanImage').css("width", "90%");

    $('#fingersToScanImage').css("height", "100%");

    $('#fingersToScanImage').css("vertical-align", "middle");

    $('#fingersToScanImage').css("border-radius", "155px");

    $('#fingersToScanImage').css("margin-top", "1px");

    $('#nistIndex').fadeIn(500);
    $('#nistIndex').html('<button class="btn btn-default btn-circle" id="btn-nist" data-toggle="modal" data-target="#modal-nist" title="Índice de calidad de imagen capturada de la huella">'+objectNotificacionHuellaLeida.extra.nist+'</button>');

    var nist_object = getNistMsj(objectNotificacionHuellaLeida.extra.nist);
    nistIndex = objectNotificacionHuellaLeida.extra.nist;
    $('#nist-msj').html(nist_object.message).attr('class', 'col-12 ' + nist_object.class);
    $('#nist-msj').fadeIn(500);
    $('#fingersToScan').fadeOut(500);

    $('#fingers1').fadeOut(500, function () {
        $(this).css({ "display": "block", "visibility": "hidden" });
    });

    $('#fingers2').fadeOut(500, function () {
        $(this).css({ "display": "block", "visibility": "hidden" });
    });

    $("#verification-feedback").show();
    $("#verification-feedback").empty().html("VERIFICANDO");

};

function funcionVerificado(message) {

    $('#verificationIcon').show();
    $('#verificationIcon').attr('src', 'img/Cube2.gif');

    var objectNotificacionVerificado = $.parseJSON(message);

    console.log("funcionVerificado");
    console.log(objectNotificacionVerificado);

    $("#verification-feedback").show();

    if (!objectNotificacionVerificado.estado) {
        if (objectNotificacionVerificado.mensaje.codigo == 101) {
           $(".feedback .header").html("VERIFICACIÓN FINALIZADA");
           clearIntervalText($(".feedback"));

           $(".feedback #msj").html('Presione el botón "Realizar otra verificación" para comenzar de nuevo.');
           $("#verification-feedback").empty().html("VERIFICACIÓN NEGATIVA");
           resultTransaction = 0;
           $('#verificationIcon').attr('src', 'img/negativo.png');

           if (genera_txt) {
            generarTXT(sobrescribe_txt);
        }
        $('#fingerContainer').css('border', '15px solid #ccc');
        $("#new-verification").show();
        $("#error").show();
        $('#verificationIcon').hide();

    }
    else {
        repetirDedo(dedo);
        var $div = $('<p></p>').html('HUELLA NO COINCIDE.').append('<br />').append('COLOQUE DEDO NUEVAMENTE');
        $("#verification-feedback").empty().html($div);
        $('#fingerContainer').css('border', '15px solid #008cba');
        $("#info").show();
        $('#verificationIcon').hide();
        $('#fingersToScan').show();
        $('#fingersToScan').empty().html(dedo);
    }
}
else {
 $(".feedback .header").html("VERIFICACIÓN FINALIZADA");
 clearIntervalText($(".feedback"));
 $(".feedback #msj").html('Presione el botón "Realizar otra verificación" para comenzar de nuevo.');
 if (objectNotificacionVerificado.extra.verificacion === "1") {
    if (alerta_fraude == true) {
        if (objectNotificacionVerificado.extra.transaccion.indexOf('f') == 0 || objectNotificacionVerificado.extra.transaccion.indexOf('F') == 0) {
            $("#verification-feedback").empty().html("VERIFICACIÓN NEGATIVA");
            $("#verification-feedback").append('<br>Alerta CI');
            $("#new-verification").show();
            if (genera_previred) {
                $("#call-previred").show();
            }
            resultTransaction = 0;
            $('#fingerContainer').css('border', '15px solid #ccc');
            id_trx = objectNotificacionVerificado.extra.transaccion;
            run = $('#scanned_rut').val();
            notificar("error", "Alerta CI");
            $("#info").hide();
            $("#error").show();
            $('#verificationIcon').hide();
        }
        else {
            if (tipo_carnet != 'antiguo' && objectNotificacionVerificado.extra.puntaje > 9990) {
                dedoUsado(objectNotificacionVerificado.extra.dedoVerificado);
                $('#verificationIcon').hide();
                var $div = $('<p></p>').html(objectNotificacionVerificado.extra.dedoVerificado).append('<br />').append('VERIFICACIÓN POSITIVA');
                $("#verification-feedback").empty().html($div);
            } else {
                dedoUsado(dedo);
                $('#verificationIcon').hide();
                var $div = $('<p></p>').html(dedo).append('<br />')
                .append('VERIFICACIÓN POSITIVA');
                $("#verification-feedback").empty().html($div);
                resultTransaction = 1;
            }
            $('#fingerContainer').css('border', '15px solid #ccc');
            $("#new-verification").show();

            if (genera_previred) {
                $("#call-previred").show();
            }

            id_trx = objectNotificacionVerificado.extra.transaccion;
            run = $('#scanned_rut').val();
            $("#info").hide();
            $("#success").show();
        }

    }
    else {
        if (tipo_carnet != 'antiguo' && objectNotificacionVerificado.extra.puntaje > 9998) {
            dedoUsado(objectNotificacionVerificado.extra.dedoVerificado);
            $('#verificationIcon').hide();
            var $div = $('<p></p>').html(objectNotificacionVerificado.extra.dedoVerificado).append('<br />').append('VERIFICACIÓN POSITIVA');
            resultTransaction = 1;
            $("#verification-feedback").empty().html($div);

        } else {
            dedoUsado(dedo);
            $('#verificationIcon').hide();
            var $div = $('<p></p>').html(dedo).append('<br />').append('VERIFICACIÓN POSITIVA');
            resultTransaction = 1;
            $("#verification-feedback").empty().html($div);
        }
        $('#fingerContainer').css('border', '15px solid #ccc');
        $("#new-verification").show();
        if (genera_previred) {
            $("#call-previred").show();
        }

        id_trx = objectNotificacionVerificado.extra.transaccion;
        run = $('#scanned_rut').val();
        $("#info").hide();
        $("#success").show();
    }
}
else if (objectNotificacionVerificado.extra.verificacion === "2") {
	 $("#verification-feedback").empty().html("CEDULA BLOQUEADA");
	 $("#verification-feedback").append('<br>Diríjase al Registro Civil.');
     $("#new-verification").show();
	 resultTransaction = 0;
     $('#fingerContainer').css('border', '15px solid #ccc');
     id_trx = objectNotificacionVerificado.extra.transaccion;
     run = $('#scanned_rut').val();
     $("#info").hide();
     $("#error").show();
     $('#verificationIcon').hide();
}
else {
    $("#verification-feedback").empty().html("VERIFICACIÓN NEGATIVA");
    resultTransaction = 0;
    $("#info").hide();
    $('#verificationIcon').hide();
    $("#error").show();
    $("#new-verification").show();
    $('#fingerContainer').css('border', '15px solid #ccc');


}
if (genera_txt) {
    generarTXT(sobrescribe_txt);
}

}
if (activa_facial) {
    if (resultTransaction === 0) {
        $("#msj-error-verificacion").html("No se ha podido verificar la identidad. ");
        detieneServicio();
        disconnect();
        $('#modal-confirm-facial').modal( 'toggle' );
    }
}
}

function funcionError(message) {

    var objectNotificacionError = $.parseJSON(message);

    console.log("funcionError");
    console.log(objectNotificacionError);
    if (objectNotificacionError.mensaje.codigo == 101) {
        restartAppNoForm();
    }
    else{
        showError();
    }
};

function funcionInformativo(message) {

    var objectNotificacionInformativo = $.parseJSON(message);

    console.log("funcionInformativo");
    console.log(objectNotificacionInformativo);

    notificar('success', objectNotificacionInformativo.mensaje.mensaje_usuario);

}



function funcionFotografiaObtenida(message) {
    console.log("> notificacionFotografiaObtenida");

    var objectFotografia = $.parseJSON(message);

    console.log("funcionFotografiaObtenida");
    // console.log(objectFotografia);
    switch (modo_foto) {
     case "frontal":
     $("#id_front").attr('src', 'data:image/png;base64,'+objectFotografia.extra.fotografia);
     break;
     case "trasera":
     $("#id_back").attr('src', 'data:image/png;base64,'+objectFotografia.extra.fotografia);

     break;
     case "selfie":
     $("#id_selfie").attr('src', 'data:image/png;base64,'+objectFotografia.extra.fotografia);

     break;
     default:
     if ($(".image").length >= 2) {
        $('#image-container').html('');
        $("#download-images").hide();
    }

    $('#image-container')
    .append('<div class="col-sm-6" style="margin-bottom: 20px;"><img alt="Embedded Image" class="image" width="300" src="data:image/png;base64,' + objectFotografia.extra.fotografia + '" /><button id="download-image' + ($(".image").length + 1) + '" class="btn btn-oscuro" style="width: 90%; margin-left: 5%; margin-top: 5px; font-weight: bold">Descargar Imagen</button></div>');

    if ($(".image").length == 2) {
        $('#image-container').append('<div style="clear:both"></div>');
    }

    $('#captura-foto').toggleClass('disabled');
    $('#capturando-foto').toggleClass('disabled');
    break;
}


}
function funcionPreviredObtenido(message) {

    var objectNotificacionPrevired = $.parseJSON(message);
    console.log("funcionPreviredObtenido");
    console.log(objectNotificacionPrevired);
    if (objectNotificacionPrevired.extra.status == "NOK") {
        console.log("tiene errores");
        console.log(objectNotificacionPrevired.extra.message);
        notificar('error', 'Ocurrio un error al consumir previred');

    }
    else {
        console.log("No tiene errores");
        window.open('http://' + objectNotificacionPrevired.extra.url);
    }
}


function funcionEstadoCIObtenido(message) {
    var objectNotificacionEstadoCIObtenido = $.parseJSON(message);

    console.log("funcionEstadoCIObtenido");
    console.log(objectNotificacionEstadoCIObtenido);
    var indVigencia = objectNotificacionEstadoCIObtenido.extra.IndVigencia;
    var vigenciaEstado = "";
    var vigenciaMsj = "El estado de su cédula es: ";
    if (indVigencia == "S") {
        vigenciaEstado = "VIGENTE";

    } else if (indVigencia == "N"){
        vigenciaEstado = "NO VIGENTE";
    }
    else{
        if(objectNotificacionEstadoCIObtenido.mensaje.mensaje_usuario){
            vigenciaMsj = objectNotificacionEstadoCIObtenido.mensaje.mensaje_usuario;
        }
        else{
            vigenciaMsj =" Ha ocurrido un error al recuperar los datos. Intente nuevamente. ";
        }

    }
    $('#estadoCIMensaje').html(vigenciaMsj + vigenciaEstado);
}


function funcionDatosEquipo(message){
    var objectNotificacionDatosEquipo = $.parseJSON(message);
    var message = '';
    if (objectNotificacionDatosEquipo.estado == true) {
        message = 'Dirección IP local: ' + objectNotificacionDatosEquipo.extra.ip + "<br>";
        message += 'Nombre equipo: ' + objectNotificacionDatosEquipo.extra.pcName + "<br>";
        message += 'Nombre usuario: ' + objectNotificacionDatosEquipo.extra.pcUser + "<br>";
        message += 'Descripción del equipo: ' + objectNotificacionDatosEquipo.extra.pcDescription + "<br>";
        message += 'Mac Address del equipo: ' + objectNotificacionDatosEquipo.extra.macAddress + "<br>";
        $("#pcInfoMessage").html(message);
    } else{
        $("#pcInfoMessage").html("Hubo un error al obtener los datos del equipo");
    }
    
}

function funcionEstadoDispositivos(message) {
    try {
        var objectNotificacionEstadoDispositivos = $.parseJSON(message);

        var estados;
        var dispositivoNotReady = false;
        var base_src = "img/";
        var positivo = base_src + "positivo.png";
        var negativo = base_src + "negativo.png";
        estados = objectNotificacionEstadoDispositivos.extra;
        $.each(estados, function (key, value) {
            if (value == true) {
                $("#" + key).attr('src', positivo);
            }
            else if (value == false) {
                dispositivoNotReady = true;
                $("#" + key).attr('src', negativo);
            }
        });


    } catch (err) {
        console.log(message);
    }

}

function funcionStreamObtenido(message) {
    console.log("> funcionStreamObtenido");
    var objectStream = $.parseJSON(message);
    console.log(objectStream);

    $('#stream-container')
    .html('')
    .append('<img alt="Embedded Image" width="200" src="data:image/png;base64,' + objectStream.extra.stream + '" />');
}



function generarTXT(sobrescribe) {
    console.log("> generar TXT");
    var parametros = "";
    $("#info-persona input").each(function (data) {
        parametros += $(this).attr('id').replace('scanned_', '') + ";";
    });
    parametros += "ipAddress; fecha; " + sobrescribe;
    TOC.invoke('escribirTXT', parametros, sobrescribe);
}

function funcionLecturaNFC(message) {
    console.log("funcionLecturaNFC");
    var objectNFC = $.parseJSON(message);
    console.log(objectNFC);
    if ("numero_bip" in objectNFC.mensaje) {
        $('#numero-bip').html(objectNFC.mensaje.numero_bip);
    } else {
        $('#numero_bip').html(objectNFC.mensaje.mensaje_usuario);
    }
}

function actualizarEstadoDispositivos() {
    var estados;
    var dispositivoNotReady = false;
    var base_src = "img/";
    var positivo = base_src + "positivo.png";
    var negativo = base_src + "negativo.png";
    $('.icon-toc.device-toc').addClass('hidden');
    $('.loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        crossdomain: true,
        cache : false,
        contentType: "application/json; charset=utf-8",
        url: "http://localhost:41369",
        dataType: "json",
        success: function (data) {
            estados = data;
            console.log(estados);
            $.each(estados, function (key, value) {
                if (value == true) {
                    $("#" + key).attr('src', positivo);
                }
                else if (value == false) {
                    dispositivoNotReady = true;
                    $("#" + key).attr('src', negativo);
                }
                $("#" + key).removeClass('hidden');
            });
            $(".loader").addClass('hidden');
            setTimeout(function () {
                actualizarEstadoDispositivos()
            }, 30000)
        }
    });


}

function restartWebSocket() {
    disconnect();
    connect(0);
}

function resetForm() {
    $("#info-persona")[0].reset();
}

function restartApp() {
 resultTransaction = "";
 nistIndex="";
 clearIntervalText($(".feedback"));
 blinkText($(".feedback"));
 restartWebSocket();
 resetForm();
 resetModalFacial();
 $("#facial").fadeOut(0);
 $("#nistIndex").css("display", "none");
 $("#nist-msj").html("");
 $("#nist-msj").fadeOut(0)
 $(".feedback .header").html("PASO 1");
 $(".feedback #msj").html("Coloque la cedula de identidad en el lector.");
 $("#fingersToScanImage").attr("src", "img/fingerprint.png");
 $('#verificationIcon').show();
 $("#verificationIcon").attr("src", "");
 $('#fingerContainer').css('border', '15px solid #ccc');
 $('#row-finger').fadeOut(0);
 $("#fingersToScanImage").attr("style", "");
 $("#success").fadeOut(0);
 $("#error").fadeOut(0);
 $("#info").fadeOut(0);
 $('#fingersToScan').fadeOut(0);
 $('#dedoUsado').fadeOut(0);
 $("#verification-feedback").fadeOut(0);
 $("#new-verification").hide();
 $("#info-facial").fadeOut(0);
 // if ($("#facial-panel").is(':visible')) {
   $("#facial-panel").hide();
   $("#dactilar").show();
   stopVideoStream();
   connect(0);
// }
if (genera_previred) {
    $("#call-previred").fadeOut();
}
}

function cleanTxtIE() {
	$("#scanned_rut").val("");
	$("#scanned_nombres").val("");
	$("#scanned_apellidos").val("");
	$("#scanned_nacionalidad").val("");
	$("#scanned_nacimiento").val("");
	$("#scanned_genero").val("");
	$("#scanned_vencimiento").val("");
	$("#scanned_serie").val("");
}


function restartAppNoForm() {
   resultTransaction = "";
   nistIndex="";
   resetModalFacial();
   clearIntervalText($(".feedback"));
   blinkText($(".feedback"));
   $("#nistIndex").fadeOut(0);
   $("#nist-msj").fadeOut(0)
   $("#facial").fadeOut(0);
   $(".feedback .header").html("PASO 1");
   $(".feedback #msj").html("Coloque la cedula de identidad en el lector.");
   $("#fingersToScanImage").attr("src", "img/fingerprint.png");
   $('#verificationIcon').show();
   $("#verificationIcon").attr("src", "");
   $('#fingerContainer').css('border', '15px solid #ccc');
   $('#row-finger').fadeOut(0);
   $("#fingersToScanImage").attr("style", "");
   $("#success").fadeOut(0);
   $("#error").fadeOut(0);
   $("#info").fadeOut(0);
   $('#fingersToScan').fadeOut(0);
   $('#dedoUsado').fadeOut(0);
   $("#verification-feedback").fadeOut(0);
   $("#new-verification").hide();


   if (genera_previred) {
    $("#call-previred").fadeOut(0);
}
}


function showError() {
    $("#verification-feedback").show();
    $("#verification-feedback").empty().html("Ocurrió un error al realizar la verificación de identidad <p style='margin-top: 10px; font-size: 18px; font-style: italic;'>Compruebe el estado de los dispositivos</p>");
    $("#info").hide();
    $("#error").show();
    $('#verificationIcon').hide();
    $("#new-verification").show();
    if (genera_previred) {
        $("#call-previred").hide();
    }
}

function notificar(tipo, mensaje) {
    $.toast({
        text: mensaje,
        showHideTransition: 'fade',
        position: 'top-center',
        hideAfter: 5000,
        icon: tipo
    });
}
