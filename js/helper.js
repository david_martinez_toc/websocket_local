var intervalText = "";
var tipo_carnet = '';
var dedo = '';
var run = '';
var id_trx = '';
var modo_foto = 'default';
var browser_version = getBrowserVersion();


$(document).ready(function() {
	if (browser_version == "Other") {
		$("#success").html('<img src="img/positivo.png" style="width: 190px" alt="Positiva">');
		$("#info").html('<img src="img/reintentar.png"  style="width: 190px" alt="Reintentar">');
		$("#error").html('<img src="img/negativo.png" style="width: 190px"  alt="Negativa">');
	}
});

function getBrowserVersion(){
	
	if (navigator.userAgent.search("Chrome") >= 0) {
		return "Chrome";
	}
	else if (navigator.userAgent.search("Firefox") >= 0) {
		return "Firefox";
	}
	else{
		return "Other";

	}
}

function blinkText(selector){
	if (!selector.hasClass('blinking')) {
		intervalText = setInterval(function() {
			selector.toggleClass('blinking');
		}, 500);
	}
}
function clearIntervalText(selector){
	selector.removeClass('blinking');
	clearInterval(intervalText);
}

$("#myModal").on("hidden.bs.modal", function () {
	$.each($(this).find('img'), function () {
		$(this).attr('src', 'img/spinner.gif');
	});
	clearInterval(interval);
});
$("#info-facial").click(function(){
	$("#modal-facial").modal("show");
})

function repetirDedo(dedo) {
	switch (dedo) {
		case "INDICE DERECHO o INDICE IZQUIERDO":
		$('#right-finger').fadeIn(500);
		$('#left-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-6');
		$('#left-finger').attr('class','col-6');

		$('#fingers1').css("visibility", "visible");
		$('#fingers1').attr('class','finger-image pull-right');
		$('#fingers2').attr('class','finger-image pull-left');
		$("#fingers1").attr("src", "img/indice_izquierdo.png");
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/indice_derecho.png");

		break;
		case "INDICE DERECHO":
		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/indice_derecho.png");
		break;
		case "INDICE IZQUIERDO":
		$('#right-finger').fadeOut(0);
		$('#fingers2').attr('class','finger-image');
		$('#left-finger').fadeIn(500);
		$('#left-finger').attr('class', 'col-12');		
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/indice_izquierdo.png");
		break;
		case "PULGAR DERECHO o PULGAR IZQUIERDO":
		$('#right-finger').fadeIn(500);
		$('#left-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-6');
		$('#left-finger').attr('class','col-6');
		$('#fingers1').attr('class','finger-image pull-right');
		$('#fingers2').attr('class','finger-image pull-left');

		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/pulgar_izquierdo.png");
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/pulgar_derecho.png");
		break
		case "PULGAR DERECHO":
		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/pulgar_derecho.png");
		break;
		case "PULGAR IZQUIERDO":
		$('#right-finger').fadeOut(0);
		$('#fingers2').attr('class','finger-image');
		$('#left-finger').fadeIn(500);
		$('#left-finger').attr('class', 'col-12');
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/pulgar_izquierdo.png");
		break;
		case "ANULAR DERECHO o ANULAR IZQUIERDO":
		$('#right-finger').fadeIn(500);
		$('#left-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-6');
		$('#left-finger').attr('class','col-6');
		$('#fingers1').attr('class','finger-image pull-right');
		$('#fingers2').attr('class','finger-image pull-left');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/anular_izquierdo.png");
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/anular_derecho.png");
		break
		case "ANULAR DERECHO":
		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/anular_derecho.png");
		break;
		case "ANULAR IZQUIERDO":
		$('#right-finger').fadeOut(0);
		$('#fingers2').attr('class','finger-image');
		$('#left-finger').fadeIn(500);
		$('#left-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/anular_izquierdo.png");
		break;
		case "MEDIO DERECHO o MEDIO IZQUIERDO":
		$('#right-finger').fadeIn(500);
		$('#left-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-6');
		$('#left-finger').attr('class','col-6');
		$('#fingers1').attr('class','finger-image pull-right');
		$('#fingers2').attr('class','finger-image pull-left');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/medio_izquierdo.png");
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/medio_derecho.png");
		break
		case "MEDIO DERECHO":
		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/medio_derecho.png");
		break;
		case "MEDIO IZQUIERDO":
		$('#right-finger').fadeOut(0);
		$('#fingers2').attr('class','finger-image');
		$('#left-finger').fadeIn(500);
		$('#left-finger').attr('class', 'col-12');
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/medio_izquierdo.png");
		break;
		case "MEÑIQUE DERECHO o MEÑIQUE IZQUIERDO":
		$('#right-finger').fadeIn(500);
		$('#left-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-6');
		$('#left-finger').attr('class','col-6');
		$('#fingers1').attr('class','finger-image pull-right');
		$('#fingers2').attr('class','finger-image pull-left');

		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/meñique_izquierdo.png");
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/meñique_derecho.png");
		break
		case "MEÑIQUE DERECHO":

		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/meñique_derecho.png");
		break;
		case "MEÑIQUE IZQUIERDO":
		$('#right-finger').fadeOut(0);
		$('#fingers2').attr('class','finger-image');
		$('#left-finger').fadeIn(500);
		$('#left-finger').attr('class', 'col-12');
		$('#fingers2').css("visibility", "visible");
		$("#fingers2").attr("src", "img/meñique_izquierdo.png");
		break;
		default:
		$('#left-finger').fadeOut(0);
		$('#fingers1').attr('class','finger-image');
		$('#right-finger').fadeIn(500);
		$('#right-finger').attr('class', 'col-12');
		$('#fingers1').css("visibility", "visible");
		$("#fingers1").attr("src", "img/unknow.png");
		break;
	}
}

function dedoUsado(dedo) {
	switch (dedo) {
		case "INDICE DERECHO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/indice_derecho.png");
		break;
		case "INDICE IZQUIERDO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/indice_izquierdo.png");
		break;
		case "PULGAR DERECHO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/pulgar_derecho.png");
		break;
		case "PULGAR IZQUIERDO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/pulgar_izquierdo.png");
		break;
		case "ANULAR DERECHO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/anular_derecho.png");
		break;
		case "ANULAR IZQUIERDO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/anular_izquierdo.png");
		break;
		case "MEDIO DERECHO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/medio_derecho.png");
		break;
		case "MEDIO IZQUIERDO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/medio_izquierdo.png");
		break;
		case "MEÑIQUE DERECHO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/meñique_derecho.png");
		break;
		case "MEÑIQUE IZQUIERDO":
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/meñique_izquierdo.png");
		break;
		default:
		$('#dedoUsado').css("display", "block");
		$("#dedoUsado").attr("src", "img/unknow.png");
		break;
	}
}

function getNistMsj(nist){
	var response = {}
	response.class = 'text-center nist_'+nist;
	switch (nist) {
		case 1:
		response.message = 'Calidad de captura de la huella muy buena';
		break;
		case 2:
		response.message = 'Calidad de captura de la huella buena';
		break;
		case 3:
		response.message = 'Calidad de captura de la huella regular';
		break;
		case 4:
		response.message = 'Calidad de captura de la huella mala';
		break;
		case 5:
		response.message = 'Calidad de captura de la huella muy mala';
		break;
		default:
		response.class = 'nist_5';
		response.message = 'Calidad de captura de la huella muy mala';
		break;
	}

	return response;
}