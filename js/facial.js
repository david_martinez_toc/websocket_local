var documentData = {
    'national identification number':'',
    'document number':'',
    'name':'',
    'family name':'',
    'gender':'',
    'nationality': '',
    'date of birth':'',
    'expiration date':''
};
var cedula_mrz = {
    'national identification number':'',
    'document number':'',
    'name':'',
    'family name':'',
    'gender':'',
    'nationality': '',
    'date of birth':'',
    'expiration date':''
};

var documentType;
var videoSelect = document.querySelector("select#videoSource");
var videoElement = document.querySelector("video");
var selectors = [videoSelect];
var code = null;

function processRawMRZ(mrzRaw) {
    var decode_mrz = mrzRaw.split('\\n');
    if (decode_mrz[0].substr(0, 5) == 'INCHL') { // new CI
        cedula_mrz['document number'] = decode_mrz[0].substr(5, 9);
        cedula_mrz['date of birth'] = decode_mrz[1].substr(0, 6);
        cedula_mrz.gender = decode_mrz[1].substr(7, 1);
        cedula_mrz['expiration date'] = decode_mrz[1].substr(8, 6);
        cedula_mrz.nationality = decode_mrz[1].substr(15, 3);
        cedula_mrz['national identification number'] = decode_mrz[1].split(/</)[0].substr(18, decode_mrz[1].split(/</)[0].length) + decode_mrz[1].split(/</)[1];
        cedula_mrz['family name'] = decode_mrz[2].split(/<</)[0].replace(/</g, ' ');
        cedula_mrz.name = decode_mrz[2].split(/<</)[1].replace(/</g, ' ');
    } else if (decode_mrz[0].substr(0, 5) == 'IDCHL') { // old CI
        cedula_mrz['national identification number'] = decode_mrz[0].substr(5, 9);
        cedula_mrz['date of birth'] = decode_mrz[1].substr(0, 6);
        cedula_mrz.gender = decode_mrz[1].substr(7, 1);
        cedula_mrz['expiration date'] = decode_mrz[1].substr(8, 6);
        cedula_mrz.nationality = decode_mrz[1].substr(15, 3);
        cedula_mrz['document number'] = decode_mrz[1].split(/</)[0].substr(18);
        cedula_mrz['family name'] = decode_mrz[2].split(/<</)[0].replace(/</g, ' ');
        cedula_mrz.name = decode_mrz[2].split(/<</)[1].replace(/</g, ' ');
    }
    // UI element MRZ RAW in 3 lines
    $('h6.mrz-raw span.one').text(decode_mrz[0]);
    $('h6.mrz-raw span.two').text(decode_mrz[1]);
    $('h6.mrz-raw span.three').text(decode_mrz[2]);
}


function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = decodeURIComponent(dataURI.split(',')[1]);
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type:'image/jpeg'});
}

function validateForm(){
    var response = {};

    if ($("#documentType").val() == 0) {
        response.is_valid = false;
        response.message = 'Debe seleccionar el tipo de documento.';
        return response;
    }
    var foto_front = $("#id_front").attr('src').replace("data:image/png;base64,", "");
    var foto_back = $("#id_back").attr('src').replace("data:image/png;base64,", "");
    var foto_selfie = $("#id_selfie").attr('src').replace("data:image/png;base64,", "");
    var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
    if (!base64Matcher.test(foto_front) || !base64Matcher.test(foto_back) || !base64Matcher.test(foto_selfie) ) {
        response.is_valid = false;
        response.message = 'Debe capturar todas las fotos solicitadas.';
        return response;
    }
    response.is_valid = true;
    response.message = 'OK';
    return response;

}

function setDocumentData(code, mrz) {
    if (code != '') {
        $.each(code.data, function(index, el) {
            documentData[index] = el;
        });
        $.each(documentData, function(key, value) {
            if (value == '') {
                documentData[key] = mrz[key];
            }
        });
    } else {
        $.each(mrz, function(index, el) {
            documentData[index] = el;
        });
    }
    return documentData;
}

function resetModalFacial(){
    $("#li_front a").trigger("click");
    $("#li_resultado_facial").fadeOut();
    $("#id_front").attr('src', 'img/ci_front.png');
    $("#id_back").attr('src', 'img/ci_back.png');
    $("#id_selfie").attr('src', 'img/front.jpg');
    $("#documentType").val($("#documentType option:first").val());
}

function gotSources(sourceInfos) {
  for (var i = 0; i != sourceInfos.length; ++i) {
    var sourceInfo = sourceInfos[i];
    var option = document.createElement("option");
    option.value = sourceInfo.id;
    if (sourceInfo.kind === 'video' && sourceInfo.label.indexOf("Doubango") === -1) {
      option.text = sourceInfo.label || 'camera ' + (videoSelect.length + 1);
      videoSelect.appendChild(option);
  }
}
}

function successCallback(stream) {
  window.stream = stream; // make stream available to console
  $("#video-panel").html('<video id="video" style="max-width:100%;padding:.55rem 0"></video>')
  videoElement = document.querySelector("video");
  attachMediaStream(videoElement, stream);
}

function errorCallback(error){
  console.log("navigator.getUserMedia error: ", error);
}


function startVideoStream(){
    var direction = '';
    var app_locale = '';
    var parser = new UAParser();
    var user_agent = parser.getResult();
    if (typeof user_agent.device.model != 'undefined' || user_agent.os.name == 'Android' || user_agent.os.name == 'iOS'
       || user_agent.os.name == 'Blackberry' || user_agent.os.name == 'Blackberry' || user_agent.os.name == 'Windows Phone'
       || user_agent.os.name == 'Windows Mobile') {
        var isMobile = false;
}


$("#preview").show();
if (!isMobile) {
    var prefix;
    var version;
    if (window.mozRTCPeerConnection || navigator.mozGetUserMedia) {
        prefix = 'moz';
        version = parseInt(navigator.userAgent.match(/Firefox\/([0-9]+)\./)[1], 10);
    } else if (window.webkitRTCPeerConnection || navigator.webkitGetUserMedia) {
        prefix = 'webkit';
        version = navigator.userAgent.match(/Chrom(e|ium)/) && parseInt(navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./)[2], 10);
    }
    var cont_camera_devices = 0;
    if (browser_version !== "Other") {
        console.log("Browser Support WebRTC")

        navigator.mediaDevices.enumerateDevices().then(function (devices) {
            for(var i = 0; i < devices.length; i ++){
                var device = devices[i];
                if (device.kind === 'videoinput') {
                    cont_camera_devices++;
                    var option = document.createElement('option');
                    option.value = device.deviceId;
                    option.text = device.label || 'Cámara ' + (cont_camera_devices);
                    document.querySelector('select#videoSource').appendChild(option);
                }
            };
            setVideoStream($('#videoSource').val(), '');
        });
        $('.card-body input[type="file"]').detach();
        $('label.images').detach();
    } else {
       console.log("Browser Does Not Support WebRTC")
       setVideoStreamIE();
   }
} else {
    $('.preview').hide();
    $('.card-body button.btn.btn-dark').detach();
}
}

var MediaStream;
function setVideoStream(deviceId) {
    var constraints = { audio: false, video: { width: 1280, height: 720, deviceId: deviceId} };
    navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream) {
        var video = document.querySelector('video');
        video.srcObject = mediaStream;
        video.onloadedmetadata = function(e) {
            video.play();
        };
    }).catch(function(err) {
       alert("Hubo un error en el streaming de video");
       console.log(err.name + ": " + err.message);
    }); // always check for errors at the end.
}

function setVideoStreamIE() {

    videoElement = document.querySelector("video");
    videoSelect = document.querySelector("select#videoSource");

    if (window.stream) {

        window.stream.getTracks().forEach(function(track) {
          track.stop();
      });
        attachMediaStream(videoElement, null);

    } else{
        MediaStreamTrack.getSources(gotSources);
    }
    var videoSource = videoSelect.value;

    var constraints = { audio: false, video: {width: 1280, height: 720,deviceId: videoSource ? {exact: videoSource} : undefined} };
    console.log(constraints)
    navigator.getUserMedia(constraints, successCallback, errorCallback);
}

function  stopVideoStream(){
   if (window.stream) {

    window.stream.getTracks().forEach(function(track) {
      track.stop();
  });
    attachMediaStream(videoElement, null);

}
}


function getDevices() {
  if (typeof Promise === 'undefined') {
    return MediaStreamTrack.getSources(gotDevices);
} else {
    return navigator.mediaDevices.enumerateDevices()
    .then(gotDevices)
    .catch(errorCallback);
}
}

function gotStream(stream) {
  window.stream = stream; // make stream available to console
  videoElement = attachMediaStream(videoElement, stream);
  // Refresh button list in case labels have become available
  return getDevices();
}


function gotDevices(deviceInfos) {
  // Handles being called several times to update labels. Preserve values.
  var values = selectors.map(function(select) {
    return select.value;
});
  selectors.forEach(function(select) {
    while (select.firstChild) {
      select.removeChild(select.firstChild);
  }
});
  for (var i = 0; i !== deviceInfos.length; ++i) {
    var deviceInfo = deviceInfos[i];
    var option = document.createElement('option');
    option.value = deviceInfo.id || deviceInfo.deviceId;
    if (deviceInfo.kind === 'videoinput' ||
       deviceInfo.kind === 'video') {
      option.text = deviceInfo.label || 'camera ' + (videoSelect.length + 1);
  videoSelect.appendChild(option);
}
}
selectors.forEach(function(select, selectorIndex) {
    if (Array.prototype.slice.call(select.childNodes).some(function(n) {
      return n.value === values[selectorIndex];
  })) {
      select.value = values[selectorIndex];
  }
});
}


$('body').on('change', 'select#videoSource', function(event) {
    // event.preventDefault();
    deviceId = $(this).val();
    if (browser_version == "Other") {
        setVideoStreamIE(deviceId);
    } else{
        setVideoStream(deviceId);
    }
});

$('body').on('click', '.take-id-front', function(event) {
    event.preventDefault();
    $('input[name="id_front"]').val("");

    var photoData = takePhoto();
    $('img#id_front').attr('src', photoData);
    var base64data = $('img#id_front').attr('src').split(',');
    $('input[name="id_front"]').val(photoData);
});

$('body').on('click', '.take-id-back', function(event) {
 event.preventDefault();
 $('input[name="id_back"]').val("");

 var photoData = takePhoto();
 $('img#id_back').attr('src', photoData);
 var base64data = photoData.split(',');
 $('input[name="id_back"]').val(photoData);
});

$('body').on('click', '.take-selfie', function(event) {
    event.preventDefault();
    $('input[name="selfie"]').val("");

    var photoData = takePhoto();
    $('img#id_selfie').attr({ src : photoData});
    var base64data = photoData.split(',');
    $('input[name="selfie"]').val(photoData);
});

function takePhoto() {
  var video = document.getElementById('video');
  var canvas = document.getElementById('photo');
  if (browser_version == "Other") {
    var base64 = video.getFrame();
    var image = new Image();

    image.setAttribute('src', 'data:image/jpeg;base64,' + base64);

    var canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;
    canvas.getContext("2d").drawImage(image, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);

    return canvas.toDataURL('image/jpeg');

} else{
  canvas.getContext('2d').drawImage(video, 0, 0, 1280, 720, 0, 0, 1280, 720);
  return canvas.toDataURL('image/jpeg');
}
}



$("body").on("click","#toggle-facial-panel", function(){
    if ($(this).find("span").attr('class').indexOf("minus") !== -1) {
        $(this).find("span").attr('class', 'glyphicon glyphicon-plus');
        $(".collapse").hide();
    } else if ($(this).find("span").attr('class').indexOf("plus") !== -1) {
        $(this).find("span").attr('class', 'glyphicon glyphicon-minus');
        $(".collapse").show();
    }
})

$('body').on('click', '.api-call', function(event) {
    $(".loading").show();
    event.preventDefault();
    cleanDataUI();
    var data_valid = false;
    if (documentType != 0) {
        var id_front = $('input[name="id_front"]').val();
        var id_back = $('input[name="id_back"]').val();
        var selfie = $('input[name="selfie"]').val();
        if (id_front.length > 0 && id_back.length > 0 && selfie.length > 0 && documentType != 'PASS') {
            data_valid = true;
        } else if (id_front.length > 0 && selfie.length > 0 && documentType == 'PASS') {
            data_valid = true;
        }
        if (data_valid) {
            var apiKey = facial_api_key;
            console.log(apiKey)
            var form_data = new FormData();
            form_data.append('id_front', dataURItoBlob(id_front));
            form_data.append('id_back', dataURItoBlob(id_back));
            form_data.append('selfie', dataURItoBlob(selfie));
            form_data.append('documentType', documentType);
            form_data.append('apiKey', apiKey);
            console.log(form_data)
            $.ajax({
                url: 'https://sandbox-api.7oc.cl/v2/face-and-document',
                async: true,
                crossDomain: true,
                processData: false,
                contentType: false,
                type: 'POST',
                data: form_data
            })
            .done(function(data) {
                documentData = {
                    'national identification number':'',
                    'document number':'',
                    'name':'',
                    'family name':'',
                    'gender':'',
                    'nationality': '',
                    'date of birth':'',
                    'expiration date':''
                };
                cedula_mrz = {
                    'national identification number':'',
                    'document number':'',
                    'name':'',
                    'family name':'',
                    'gender':'',
                    'nationality': '',
                    'date of birth':'',
                    'expiration date':''
                };
                console.log(documentData);
                console.log(data)
                $('.loading').hide();
                console.log(data);
                var status_class = '';
                var result_class = '';
                var result_text = '';
                var result_text_locale = '';
                var mrz = null;
                var ocr = null;
                var type = null;
                var checksum_result = '';
                var checksum_result_locale = '';
                if (typeof(data["information from document"]) != 'undefined' && data["information from document"].length != 0) {
                    mrz = data["information from document"].mrz;
                    console.log(data["information from document"]);
                    type = data["information from document"].type;
                    console.log(data["information from document"])
                    $("#verification-feedback").css("display", "block");
                    $("#new-verification").css("display", "block");

                    if (parseFloat(data["biometric result"]) > 1) {
                        result_class = 'btn btn-pill btn-success';
                        result_text = '<i class="fa fa-check mr-1"></i>' + "POSITIVO";
                        result_text_locale = 'positive';
                        $(".feedback #msj").html('Se ha verificado correctamente');
                        $("#verification-feedback").empty().html("VERIFICACIÓN POSITIVA");
                        $("#success").css("display", "block");
                        $("#error").hide();
                        $("#info").hide();

                    } else if (parseFloat(data["biometric result"]) == 1) {
                        result_class = 'btn btn-pill btn-warning';
                        result_text = "POSITIVO";
                        result_text_locale = 'positive';

                        $(".feedback #msj").html('Se ha verificado correctamente');
                        $("#verification-feedback").empty().html("VERIFICACIÓN POSITIVA");
                        $("#success").css("display", "block");
                        $("#error").hide();
                        $("#info").hide();

                    } else if (parseFloat(data["biometric result"]) < 0) {
                        result_class = 'btn btn-pill btn-warning';
                        result_text = "SIN ROSTRO";
                        result_text_locale = 'no_face';
                        $(".feedback #msj").html('Tome nuevamente las fotografías para continuar.');
                        $("#verification-feedback").empty().html("SIN ROSTRO");
                        $("#info").css("display", "block");
                        $("#error").hide();
                        $("#success").hide();

                    } else if (parseFloat(data["biometric result"]) == 0) {
                        result_class = 'btn btn-pill btn-danger';
                        result_text = '<i class="fa fa-remove mr-1"></i>' + "NEGATIVO";
                        result_text_locale = 'negative';
                        $(".feedback #msj").html('Presione el botón "Realizar otra verificación" para comenzar de nuevo.');
                        $("#verification-feedback").empty().html("VERIFICACIÓN NEGATIVA");
                        $("#error").css("display", "block");
                        $("#info").hide();
                        $("#success").hide();

                    }
                    if (data.status == '200' || data.status == '201' || data.status == '202') {
                        status_class = 'text-success';
                    } else {
                        status_class = 'text-danger';
                    }
                    $('.card.match a.confidence').append(result_text).addClass(result_class).attr('data-localize', result_text_locale);
                    $('.card.match p.token span.badge-light').text(data.toc_token);
                    if (mrz != null && typeof(mrz) != 'undefined' && typeof(mrz.checksum) != 'undefined') {
                        if (mrz.checksum == '1') {
                            checksum_result = "DOCUMENTO VALIDADO" + '_text-success';
                            checksum_result_locale = 'checksum_positive';
                        } else {
                            checksum_result = "NO SE PUDO VALIDAR DOCUMENTO" + '_text-warning';
                            checksum_result_locale = 'checksum_negative';
                            if (typeof(mrz.raw) != 'undefined') {
                                processRawMRZ(mrz.raw);
                                $('h6.mrz-raw').show();
                            }
                        }
                    } else {
                        checksum_result = "NO SE PUDO VALIDAR DOCUMENTO" + '_text-warning';
                        checksum_result_locale = 'checksum_negative';
                    }
                    checksum_result = checksum_result.split('_');
                    $('p.mrz-check span.verify-doc').text(checksum_result[0]).addClass(checksum_result[1]).attr('data-localize', checksum_result_locale);
                    // check old CI
                    if (typeof(data["information from document"].code) != 'undefined' && data["information from document"].code.length != 0) {
                        code = data["information from document"].code;
                        if (typeof(code["document status"]) != 'undefined' && code["document status"].length != 0) {
                            $('a.link-rc').attr({href: code["document status"], target: '_blank'}).show();
                        }
                        if (documentType == 'DCAN') {
                            $('.card.match .card-body p.mrz-check').hide();
                            $('.card.match .card-body ul.list-group-flush.mrz-data').hide();
                            $.each(code.data, function(index, el) {
                                var data = '<li class="list-group-item">' + index + ': <strong>' + el + '</strong></li>';
                                $('.card.match .card-body ul.list-group-flush.data').append(data);
                            });
                            $('.card.match .card-body ul.list-group-flush.data').show();
                        } else if (documentType == 'GBR1') {
                            $('.card.match .card-body p.mrz-check').hide();
                            $('.card.match .card-body ul.list-group-flush.mrz-data').hide();
                            $.each(ocr.data, function(index, el) {
                                var data = '<li class="list-group-item">' + index + ': <strong>' + el + '</strong></li>';
                                $('.card.match .card-body ul.list-group-flush.data').append(data);
                            });
                            $('.card.match .card-body ul.list-group-flush.data').show();
                        } else {
                            if (mrz != null && typeof(mrz.data) != 'undefined') {
                                setDocumentData(code, mrz.data);
                            } else {
                                setDocumentData(code, cedula_mrz);
                            }
                        }
                    } else {
                        if (mrz != null && typeof(mrz.data) != 'undefined') {
                            setDocumentData('', mrz.data);
                        } else {
                            setDocumentData('', cedula_mrz);
                        }
                    }
                    console.log("data: "+ documentData["national identification number"]);
                    $('li.list-group-item.rut strong').text(documentData["national identification number"]);
                    $('li.list-group-item.n-serie strong').text(documentData["document number"]);
                    $('li.list-group-item.names strong').text(documentData.name);
                    $('li.list-group-item.lastnames strong').text(documentData["family name"]);
                    $('li.list-group-item.gender strong').text(documentData.gender);
                    $('li.list-group-item.country strong').text(documentData.nationality);
                    $('li.list-group-item.date-birth strong').text(documentData["date of birth"]);
                    $('li.list-group-item.date-expire strong').text(documentData["expiration date"]);
                    $("#info-facial").show();

                    setFeedbackMessage(data["biometric_result"], documentData["national identification number"]);

                } else {
                    alert("Ocurrió un error al consultar la API Facial. Por favor, inténtelo nuevamente.");
                }
                $('.loading').hide();

                console.log(documentData);
            }).fail(
            function(data) {
                console.log(data);
                console.log("error");
                $('.loading').hide();
                alert("Hubo un error en el consumo de la API");
            });
        } else {
            $('.loading').hide();
            alert("Alguna de las 3 imágenes no tiene datos válidos.");
        }
    } else {
        $('.loading').hide();
        alert("No ha seleccionado el tipo de documento.");
    }
});

function setFeedbackMessage(biometric_result, rut){
   if (rut.replace("-", "").replace(" ", "").trim() !== rutTransaction.replace("-", "").trim()) {
    $(".feedback #msj").html('Presione el botón "Realizar otra verificación" para comenzar de nuevo.');
    $("#verification-feedback").empty().html("RUT NO COINCIDE");
    $("#error").hide();
    $("#info").css("display", "block");
    $("#success").hide();
    alert("El rut de la verificación facial no corresponde a la dactilar.")
}  else {
    if (parseFloat(biometric_result) > 1) {

        $(".feedback #msj").html('Se ha verificado correctamente');
        $("#verification-feedback").empty().html("VERIFICACIÓN POSITIVA");
        $("#success").css("display", "block");
        $("#error").hide();
        $("#info").hide();

    } else if (parseFloat(biometric_result) == 1) {
        $(".feedback #msj").html('Se ha verificado correctamente');
        $("#verification-feedback").empty().html("VERIFICACIÓN POSITIVA");
        $("#success").css("display", "block");
        $("#error").hide();
        $("#info").hide();

    } else if (parseFloat(biometric_result) < 0) {

        $(".feedback #msj").html('Tome nuevamente las fotografías para continuar.');
        $("#verification-feedback").empty().html("SIN ROSTRO");
        $("#info").css("display", "block");
        $("#error").hide();
        $("#success").hide();

    } else if (parseFloat(biometric_result) == 0) {

        $(".feedback #msj").html('Presione el botón "Realizar otra verificación" para comenzar de nuevo.');
        $("#verification-feedback").empty().html("VERIFICACIÓN NEGATIVA");
        $("#error").css("display", "block");
        $("#info").hide();
        $("#success").hide();

    }

}

}

function cleanDataUI() {
    // reset driving License
    $("#feedbackFacial").html("");
    $('.card.match .card-body p.mrz-check').show();
    $('.card.match .card-body ul.list-group.mrz-data').show();
    $('.card.match .card-body ul.list-group.data').hide();
    // clean
    if ($('.card.match .confidence').hasClass('btn-success')) {
        $('.card.match .confidence').removeClass('btn-success');
    } else if ($('.card.match .confidence').hasClass('btn-warning')) {
        $('.card.match .confidence').removeClass('btn-warning');
    } else {
        $('.card.match .confidence').removeClass('btn-danger');
    }
    $('.card.match .confidence').empty();
    if ($('.card.match h6 span.status').hasClass('text-success')) {
        $('.card.match h6 span.status').removeClass('text-success');
    } else {
        $('.card.match h6 span.status').removeClass('text-danger');
    }
    $('.card.match h6 span.status').text('');
    $('.card.match p.token span.badge-light').text('');
    $('p.mrz-check span.verify-doc').text('');
    if ($('p.mrz-check span.verify-doc').hasClass('text-success')) {
        $('p.mrz-check span.verify-doc').removeClass('text-success');
    } else {
        $('p.mrz-check span.verify-doc').removeClass('text-warning');
    }
    $('h6.mrz-raw span.one').text('');
    $('h6.mrz-raw span.two').text('');
    $('h6.mrz-raw span.three').text('');
    $('h6.mrz-raw').hide();
    $('a.link-rc').attr('href', '#').hide();
    $('li.list-group-item.rut').show();
    $('li.list-group-item.rut strong').text('');
    $('li.list-group-item.n-serie strong').text('');
    $('li.list-group-item.names strong').text('');
    $('li.list-group-item.lastnames strong').text('');
    $('li.list-group-item.gender strong').text('');
    $('li.list-group-item.country strong').text('');
    $('li.list-group-item.date-birth strong').text('');
    $('li.list-group-item.date-expire strong').text('');

}
$('body').on('click', '.rotate', function(event) {
    event.preventDefault();
    rotatedBase64 = '';
    direction = $(this).attr('data-dir') === "right" ? true : false;
    input_name = $(this).parent().siblings('input[type="text"]').attr('name');
    img_id = $(this).parent().siblings('img.img-fluid').attr('id');
    imageSrc = $(this).parent().siblings('img.img-fluid').attr('src');
    if (imageSrc.length > 0) {
        $('input[name="' + input_name + '"]').val('');
        var rotated = rotate64($('#'+img_id).attr('src'), direction );
        if (img_id == 'selfie') {
            $('img#selfie').attr({src: rotated});
        } else {
            $('img#' + img_id).attr('src', rotated);
        }
        $('input[name="' + input_name + '"]').val(rotated);

    }
});

function rotate64(base64Image, isClockwise) {
    var offScreenCanvas = document.createElement('canvas');
    offScreenCanvasCtx = offScreenCanvas.getContext('2d');

    var img = new Image();
    img.src = base64Image;

    offScreenCanvas.height = img.width;
    offScreenCanvas.width = img.height;

    if (isClockwise) {
        offScreenCanvasCtx.rotate(90 * Math.PI / 180);
        offScreenCanvasCtx.translate(0, -offScreenCanvas.width);
    } else {
        offScreenCanvasCtx.rotate(-90 * Math.PI / 180);
        offScreenCanvasCtx.translate(-offScreenCanvas.height, 0);
    }
    offScreenCanvasCtx.drawImage(img, 0, 0);

    return offScreenCanvas.toDataURL("image/jpeg", 100);
}
